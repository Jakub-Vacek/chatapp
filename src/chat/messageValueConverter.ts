export class MessageValueConverter {
    public toView(value: number): string {
        if ((!value) || (value === 0)) {
            return '';
        }
        if (value >= 10) {
            return '10+';
        }
        return value.toString();
    }
}
