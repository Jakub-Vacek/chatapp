import * as moment from 'moment';
export class DateFormatValueConverter {
    public toView(value: number): string {
        moment.locale('cs');
        return moment(value).format('D. MM hh:mm');
    }
}
