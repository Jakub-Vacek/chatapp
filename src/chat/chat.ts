import * as defaultConfig from '../../dist/defaultConfig.json';
import { Service } from '../service/service';
import { Room } from '../model/room';
import { Message } from '../model/message';
import { ControllerValidateResult, validateTrigger, ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';
import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';
//Emojis
//@ts-ignore
import EmojiPicker from 'rm-emoji-picker';

@autoinject

export class Chat {
    private controller: ValidationController;
    private router: Router;
    private username!: string;
    private rooms!: Room[];
    private service: Service;
    //Messages (sorted according to username)
    private messChunks!: Message[][];
    private numberOfMessages!: number;
    //View model
    private activeRoomIndex!: number;
    public activeRoomSelected: boolean = false;
    public message!: string;
    public lastUpdate!: number;
    //Update messages timer
    public updateMessagesTimer!: any;
    //Update unread messages timer
    public updateRoomsTimer!: any;
    //Scroll
    public scrollTopFired: boolean = false;
    //Emoji
    public picker: EmojiPicker;
    public constructor(controller: ValidationControllerFactory, router: Router) {
        this.router = router;
        this.controller = controller.createForCurrentScope();
        this.controller.validateTrigger = validateTrigger.manual;
        this.controller.addObject(this);
        ValidationRules.ensure('message')
            .required()
            .then()
            .maxLength(defaultConfig.maxUsernameLenght)
            .on(Chat);
        this.service = new Service();
        this.lastUpdate = 0;
        this.numberOfMessages = 0;
        this.loadUsername();
        this.getRooms();
    }

    /*
    Loads username from local storage
     */
    private loadUsername(): void {
        const result: any = localStorage.getItem(defaultConfig.usernameKey);
        if (result) {
            this.username = JSON.parse(result);
        }
    }
    /*
    Sorts messages according to username
     */
    private sortMessages(newMessages: Message[]): void {
        let position: number;
        let activeUsername: string;
        //First loading - init messChunks
        if (this.messChunks.length === 0) {
            this.messChunks = [];
            position = 0;
            activeUsername = newMessages[0].username;
            this.messChunks.push([]);
            for (const m of newMessages) {
                if (activeUsername === m.username) {
                    this.messChunks[position].push(m);
                } else {
                    activeUsername = m.username;
                    position++;
                    this.messChunks.push([]);
                    this.messChunks[position].push(m);
                }
            }//Loading new messages
        } else if (newMessages[0].createdOn > this.messChunks[this.messChunks.length - 1][0].createdOn) {
            position = this.messChunks.length - 1;
            activeUsername = this.messChunks[this.messChunks.length - 1][0].username;
            for (const m of newMessages) {
                if (activeUsername === m.username) {
                    this.messChunks[position].push(m);
                } else {
                    activeUsername = m.username;
                    position++;
                    this.messChunks.push([]);
                    this.messChunks[position].push(m);
                }
            } //Loading old messages
        } else {
            position = 0;
            activeUsername = newMessages[0].username;
            this.messChunks.splice(0, 0, []);
            for (const m of newMessages) {
                if (activeUsername === m.username) {
                    this.messChunks[position].push(m);
                } else {
                    activeUsername = m.username;
                    position++;
                    this.messChunks.splice(position, 0, []);
                    this.messChunks[position].push(m);
                }
            }
        }
    }
    /*
    Gets new messages from server
     */
    private async getMessages(roomId: number, limit: number, createdSince: number, fetchOldMessages: boolean): Promise<void> {
            let newMessages: Message[] = [];
            //Sort new messages
            newMessages = (await this.service.getMessages(roomId, limit, createdSince)).sort((n1: Message, n2: Message ): number => {
                if (n1.createdOn > n2.createdOn) { return 1; }
                if (n1.createdOn < n2.createdOn) { return -1; }
                return 0;
            });
            if ((this.numberOfMessages - newMessages.length) !== 0) { //We have new messages (loading old messages)
                if (newMessages.length !== 0) {//We have new messages (loading new messages)
                    if (fetchOldMessages) {
                        newMessages.splice(newMessages.length - this.numberOfMessages, this.numberOfMessages);
                    } else {
                        this.lastUpdate = newMessages[newMessages.length - 1].createdOn;
                    }
                    this.numberOfMessages += newMessages.length;
                    this.sortMessages(newMessages);
                }

            } else {
                this.scrollTopFired = true; //Prevent loading messages on scroll
            }
    }

    /*
    Gets available rooms
    */
    private async getRooms(): Promise<void> {
        try {
            this.rooms = await this.service.getRooms();
            await this.getUnreadMessages();
        } catch (e) {
            console.debug(e);
        }
    }
    /*
    Gets unread messages in pasive rooms
     */
    private async getUnreadMessages(): Promise<void> {
        try {
            let nM: Message[];
            for (const r of this.rooms) {
                if (this.activeRoomSelected) {
                    if (this.rooms[this.activeRoomIndex].id === r.id) {
                        r.numberOfUnreadMessages = 0;
                        r.lastSeen = Date.now();
                        continue;
                    }
                }
                if (!r.lastSeen) {
                    r.lastSeen = 0;
                }
                nM = await this.service.getMessages(r.id, defaultConfig.initialLoadLimit, r.lastSeen);
                r.numberOfUnreadMessages = nM.length;
            }
        } catch (e) {
         console.error(e);
        }
    }
    /*
    Updating messages
    */
    private startUpdateMessages(): void {
        if (this.updateMessagesTimer !== null) {
            clearInterval(this.updateMessagesTimer);
        }
        this.updateMessagesTimer = setInterval(async () => {
            await this.getMessages(this.rooms[this.activeRoomIndex].id, defaultConfig.updateLimit, this.lastUpdate, false);
        },                                     defaultConfig.updateInterval);
    }
    /*
    Updating rooms
    */
    private startUpdateRooms(): void {
        if (this.updateRoomsTimer !== null) {
            clearInterval(this.updateRoomsTimer);
        }
        this.updateRoomsTimer = setInterval(async () => {
            await this.getUnreadMessages();
        },                                  defaultConfig.updateRoomsInterval);
    }
    /*
    VIEW-MODEL
     */
    /*
    Selects active room
     */
    public async selectRoom(roomIndex: number): Promise<void> {
        this.activeRoomSelected = false;
        this.activeRoomIndex = roomIndex;
        this.messChunks = [];
        this.lastUpdate = 0;
        this.numberOfMessages = 0;
        await this.getMessages(this.rooms[this.activeRoomIndex].id, defaultConfig.initialLoadLimit, this.lastUpdate, false);
        this.rooms[this.activeRoomIndex].numberOfUnreadMessages = 0;
        this.rooms[this.activeRoomIndex].lastSeen = Date.now();
        this.activeRoomSelected = true;
        this.startUpdateMessages();
        this.startUpdateRooms();
        this.activeRoomSelected = true;
        this.activeRoomIndex = roomIndex;
    }
    /*
    Sends message
     */
    public async sendMessage(): Promise<void> {
        const result: ControllerValidateResult = await this.controller.validate();
        if (result.valid) {
            try {
                if (await this.service.postMessage(this.message, this.username, this.rooms[this.activeRoomIndex].id)) {
                    this.message = '';
                }
            } catch (e) {
                console.error(e);
            }
        }
    }
    /*
    Enter press
     */
    public keypress(event: any): boolean {
        if (event.which === 13) {
            this.sendMessage();
            event.preventDefault();
            return false;
        }
        return true;
    }
    /*
    Log out
     */
    public async logout(): Promise<void> {
        clearInterval(this.updateMessagesTimer);
        clearInterval(this.updateRoomsTimer);
        localStorage.removeItem(defaultConfig.usernameKey);
        await this.router.navigateToRoute('login');
    }
    /*
    scrollTop
     */
    public async handleScrollEvent(event: any): Promise<void> {
        if ((event.target.scrollTop === 0) && (!this.scrollTopFired)) {
            this.scrollTopFired = true;
            await this.getMessages(this.rooms[this.activeRoomIndex].id, this.numberOfMessages + defaultConfig.initialLoadLimit, 0, true);
            this.scrollTopFired = false;
        }
    }
    /*
    Initialize emoji picker
     */
    public initPicker(): void {
        this.picker = new EmojiPicker();
        this.picker._callback = (): any => {
            this.message = this.picker.getText();
        };
        this.picker.listenOn(document.getElementById('emojiButton'), document.getElementById('input'), document.getElementById('messageInput'));

    }
}
