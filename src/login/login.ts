import { ControllerValidateResult, validateTrigger, ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';
import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';
import * as defaultConfig from '../../dist/defaultConfig.json';

@autoinject

export class Login {
    private controller: ValidationController;
    private router: Router;
    private username!: string;

    public constructor(controller: ValidationControllerFactory, router: Router) {
        this.router = router;
        this.controller = controller.createForCurrentScope();
        this.controller.validateTrigger = validateTrigger.changeOrBlur;
        this.controller.addObject(this);
        ValidationRules.ensure('username')
            .required()
            .then()
            .maxLength(defaultConfig.maxUsernameLenght)
            .on(Login);
    }
    /*
    Saves username to local storage
     */
    private saveUsername(): void {
        localStorage.setItem(defaultConfig.usernameKey, JSON.stringify(this.username));
    }
    /*
    Submits login form
     */
    public async submit(): Promise<void> {
        const result: ControllerValidateResult = await this.controller.validate();
        if (result.valid) {
            this.saveUsername();
            await this.router.navigateToRoute('chat');
        }
    }
    /*
    Enter press
     */
    public keypress(event: any): boolean {
        if (event.which === 13) {
            this.submit();
            event.preventDefault();
            return false;
        }
        return true;
    }

}
