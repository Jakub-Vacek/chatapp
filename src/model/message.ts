export class Message {
    public readonly id!: number;
    public readonly username!: string;
    public readonly message!: string;
    public readonly createdOn!: number;
    public readonly roomId!: number;
}
