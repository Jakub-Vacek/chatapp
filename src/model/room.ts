export class Room {
    public id!: number;
    public name!: string;
    public lastSeen!: number;
    public numberOfUnreadMessages!: number;
}
