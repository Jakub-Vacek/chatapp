import { HttpClient, json } from 'aurelia-fetch-client';
import { Room } from '../model/room';
import { Message } from '../model/message';
import * as defaultConfig from '../../dist/defaultConfig.json';

export class Service {
    private httpClient: HttpClient;

    public constructor() {
        this.httpClient = new HttpClient();
        this.configure();
    }
    private configure(): void {
        this.httpClient.configure((config: any) => {
            config
                .useStandardConfiguration()
                .withBaseUrl('https://api.uai.urbec.org/chat/v1/')
                .withDefaults({
                    credentials: 'same-origin',
                    headers: {
                        Authorization: 'Bearer uai-624'
                    }
                });
        });
    }
    public async getRooms(): Promise<Room[]> {
        const response: Response = await this.httpClient.fetch('rooms', {
            method: 'get'
        });
        const responseJson: any = await response.json();
        return responseJson.rooms;
    }
    public async postMessage(message: string, username: string, roomId: number): Promise<boolean> {
        if ((username.length < 1) || (username.length > defaultConfig.maxUsernameLenght)) {
            return false;
        }
        if ((message.length < 1) || (message.length > defaultConfig.maxMessageLenght)) {
            return false;
        }
        const m: {} = {username: username, message: message};
        const url: string = 'https://api.uai.urbec.org/chat/v1/rooms/' + roomId + '/messages';
        const response: Response = await this.httpClient
            .fetch(url, {
                method: 'post',
                body: json(m)
            });
        return response.ok;
    }

    public async getMessages(roomId: number, limit: number, createdSince: number): Promise<Message[]> {
        const url: URL = new URL('https://api.uai.urbec.org/chat/v1/rooms/' + roomId + '/messages');
        const params: any = {limit: limit, createdSince: createdSince };
        Object.keys(params).forEach((key: string) => {url.searchParams.append(key, params[key]); });
        const response: Response = await this.httpClient
            .fetch(url.href, {
                method: 'get'
            });
        const responseJson: any = await response.json();
        return responseJson.messages;
    }
}
