import { Aurelia, PLATFORM } from 'aurelia-framework';


export function configure(aurelia: Aurelia): any {
    aurelia.use
        .standardConfiguration()
        .developmentLogging()
        .plugin(PLATFORM.moduleName('aurelia-validation'));
    aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
