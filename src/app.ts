import './app.css';
import { Router, RouterConfiguration, NavigationInstruction, Next, Redirect } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import * as defaultConfig from '../dist/defaultConfig.json';

export class App {
    // @ts-ignore
    public router: Router;

    public configureRouter(routerConfiguration: RouterConfiguration, router: Router): void {
        routerConfiguration.title = 'Chat';
        routerConfiguration.addAuthorizeStep(AuthorizeStep);
        routerConfiguration.map([
            {route: ['', 'login'], name: 'login', moduleId: PLATFORM.moduleName('./login/login'), nav: true, title: 'Login'},
            {route: 'chat', name: 'chat', moduleId: PLATFORM.moduleName('./chat/chat'), title: 'Chat', settings: {auth: true}}
        ]);

        this.router = router;
    }
}
//Very simple authorization - if there is username stored in local storage, user is authorized. In other case user is redirected to login
class AuthorizeStep {
    public run(navigationInstruction: NavigationInstruction, next: Next): Promise<any> {
        if (navigationInstruction.getAllInstructions().some((i: any) => i.config.settings.auth)) {
            const result: any = localStorage.getItem(defaultConfig.usernameKey);
            if (!result) {
                return next.cancel(new Redirect('login'));
            }
        }
        return next();
    }
}
