import { Router, RouterConfiguration } from 'aurelia-router';
export declare class App {
    router: Router;
    configureRouter(routerConfiguration: RouterConfiguration, router: Router): void;
}
